<?php

namespace App\Http\Controllers;

use App\Imports\GamificationImport;
use App\Models\Gamification;
use Illuminate\Http\Request;
use DB;
use Maatwebsite\Excel\Facades\Excel;


class GamificationController extends Controller
{
    
    public function index(Request $request)
    {
        try {
            $total = $request->itemPerPage ? $request->itemPerPage : 10;
            $data = Gamification::select("*");
            $data->when($request->input('search'), function($q) use($request) {
                $q->orWhere(DB::raw('lower(name_character)'), 'like', '%' . strtolower($request->search) . '%');
            });
            return response()->success($data->paginate($total), 200, "sukses");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
    }

    
    public function create()
    {
        //
    }

    
    public function store(Request $request)
    {
        $data = $request->all();
        try {
            $payload = Gamification::create($data);
            return response()->success($payload, 200, "sukses");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
    }

    
    public function show($id)
    {
        try {
            $group = Gamification::find($id);
            return response()->success($group, 200, "sukses");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
    }

    
    public function edit(gamification $gamification)
    {
        //
    }

    public function update(Request $request, $id)
    {
        try {
            $payload = Gamification::find($id);
            $payload->update($request->all());
            return response()->success($payload, 200, "sukses update");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
    }

    
    public function destroy($id)
    {
        try {
            $group = Gamification::find($id);
            $group->delete();
            return response()->success($group, 200, "sukses delete");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
    }

    public function uploadGamification(Request $request)
    {
        try {
            Excel::import(new GamificationImport, $request->file('file'));
            return response()->success([], 200, "Import successful");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
    }

    public function insertMultiple(Request $request)
    {
        try {
            $data = [];
            foreach ($request->gamifications as $value) {
                $data[] = [
                    'name_character' => $value['name_character'],
                    'strength_power' => $value['strength_power'],
                ];
            }
            $payload = Gamification::upsert($data, ['id'], ['name_character', 'strength_power']);
            return response()->success($payload, 200, "sukses");
        } catch (\Throwable $th) {
            return response()->error($th->getCode(), $th->getMessage());
        }
        
    }
}
