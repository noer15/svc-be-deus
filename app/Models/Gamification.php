<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Gamification extends Model
{
    use HasFactory;
    protected $fillable = ["name_character","strength_power"];
    protected $keyType = 'string';
    public $incrementing = false;
}
