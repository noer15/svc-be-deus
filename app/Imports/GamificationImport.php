<?php

namespace App\Imports;

use App\Models\Gamification;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class GamificationImport implements ToModel, WithHeadingRow
{
    
    public function model(array $row)
    {
        return new Gamification([
            "name_character" => $row['name_character'],
            "strength_power" => $row['strength_power'],
        ]);
    }
}
