<?php

use App\Http\Controllers\GamificationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/gamification', GamificationController::class);
Route::post('/uploadGamification', [GamificationController::class,'uploadGamification']);
Route::post('/insertMultiple', [GamificationController::class,'insertMultiple']);

